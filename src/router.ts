import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from './pages/Home';
import DetalheTarefa from './pages/DetalheTarefa';
import NotFound from './pages/NotFound';


Vue.use(VueRouter);

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Home,
            meta: {
                title: 'Home'
            }
        },
        {
            path: '/detalhe',
            component: DetalheTarefa,
            name: 'detalhe',
            props: true,
            meta: {
                title: 'Detalhe'
            }
        },
        {
            path: '/404',
            name: '404',
            component: NotFound,
            meta: {
                title: '404'
            }
        }
    ]
});

