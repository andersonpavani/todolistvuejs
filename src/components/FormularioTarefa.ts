import Vue from 'vue';

function sleep(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export default Vue.component('form-tarefa', {
    template: 
    /* html */ `
        <form>
            <v-container grid-list-md>
                <h2>{{ indiceEdicao != null ? 'Editar': 'Nova'}} Tarefa</h2>
                <v-layout row wrap>
                    <v-flex xs12 sm4>
                        <v-text-field
                            filled
                            :disabled="carregando"
                            name="titulo"
                            label="Título da Tarefa" 
                            v-validate="'required'" 
                            v-model="tarefa.titulo"
                            :error-messages="errors.collect('titulo')">
                            </v-text-field>
                            </v-flex>
                            
                        <v-flex xs12 sm4>
                        <v-text-field 
                            filled
                            :disabled="carregando"
                            name="descricao"
                            v-validate="'required'"
                            label="Descrição da Tarefa" 
                            v-model="tarefa.descricao"
                            :error-messages="errors.collect('descricao')">
                        </v-text-field>
                    </v-flex>
                            
                    <v-flex xs12 sm4>
                        <v-menu
                            v-model="datepicker"
                            :close-on-content-click="false"
                            :nudge-right="0"
                            transition="scale-transition"
                            offset-y
                            full-width
                            min-width="290px">
                            <template v-slot:activator="{on}">
                                <v-text-field 
                                    filled
                                    readonly
                                    :disabled="carregando"
                                    type="date"
                                    name="prazo"
                                    v-validate="'required'"
                                    label="Prazo da Tarefa" 
                                    v-model="tarefa.prazo"
                                    :error-messages="errors.collect('prazo')"
                                    v-on="on">
                                </v-text-field>
                            </template>
                            <v-date-picker 
                                v-model="tarefa.prazo"
                                @input="datepicker = false">
                            </v-date-picker>
                        </v-menu>
                    </v-flex>
                        
                </v-layout>
                <v-layout justify-end>
                    <v-btn color="success" :loading="carregando" @click.prevent='salvar()'>Salvar</v-btn>
                    <v-btn text :disabled="carregando" color="error" @click.prevent='cancelar()'>Cancelar</v-btn>
                </v-layout>
            </v-container>
        </form>
    `,
    data() {
        return {
            datepicker: false,
            carregando: false
        }
    },
    methods: {
        async salvar() {
            
            let valido = await this.$validator.validate();
            
            if (!valido){
                this.$store.dispatch('alertas/showErrorSnackBar', 'Preencha todos os campos obrigatórios');
                return;
            }

            this.carregando = true;
            await sleep(2000);

            this.$store.dispatch('tarefas/salvarTarefa', this.tarefa);
            this.cancelar();
            this.carregando = false;

            this.$store.dispatch('alertas/showSuccessSnackBar', 'Tarefa salva com sucesso')
        },
        cancelar() {
            this.tarefa = {};
            this.$store.dispatch('tarefas/limparEdicao');
            this.$emit('voltar');
        }
    },
    computed: {
        indiceEdicao() {
            return this.$store.state.tarefas.indiceEdicao;
        },
        tarefa: {
            get() {
                return this.$store.getters['tarefas/getTarefaEdicao'];
            },
            set(tarefaAlterada) {
                console.log(tarefaAlterada);
            }
        }
    }
})