import Vue from 'vue';
import TarefaService from '../service/TarefaService';
import FormatterUtil from '../util/FormatterUtil';

export default Vue.component('tabela-tarefas', {
    template: 
    /* html */ `
        <div class="mx-3" style="border=1px solid gray; border-radius=5px; overflow: hide">
        <v-simple-table>
            <thead>
                <th>Título</th>
                <th>Descrição</th>
                <th>Prazo</th>
                <th>Finalizado</th>
                <th>Ações</th>
            </thead>
            <tbody>
                <tr v-for='(t, i) in tarefas'>
                    <td>{{t.titulo}}</td>
                    <td>{{t.descricao}}</td>
                    <td>{{FormatterUtil.formatarData(t.prazo)}}</td>
                    <td>
                        <input type="checkbox" v-model='t.finalizado' @change='marcarTarefa'>
                    </td>
                    <td>
                        <v-tooltip bottom>
                            <template v-slot:activator="{on}">
                                <v-btn text icon v-on="on" color="primary" @click="visualizar(i)">
                                    <v-icon>mdi-eye</v-icon>
                                </v-btn>
                            </template>
                            <span>Visualizar</span>
                        </v-tooltip>
                        <v-tooltip bottom>
                            <template v-slot:activator="{on}">
                                <v-btn text icon color="green" @click="editar(i)">
                                    <v-icon>mdi-pencil</v-icon>
                                </v-btn>
                            </template>
                            <span>Editar</span>
                        </v-tooltip>
                        <v-tooltip bottom>
                            <template v-slot:activator="{on}">
                                <v-btn text icon color="red" @click="remover(i)">
                                    <v-icon>mdi-delete</v-icon>
                                </v-btn>
                            </template>
                            <span>Remover</span>
                        </v-tooltip>
                    </td>
                </tr>
            </tbody>
        </v-simple-table>
        </div>
    `,
    data() {
        return {
            FormatterUtil: FormatterUtil
        }
    },
    methods: {
        marcarTarefa() {
            TarefaService.atualizarLista(this.tarefas);
        },
        visualizar(i: number) {
            this.$router.push({ name: 'detalhe', params: { tarefaSelecionada: this.tarefas[i] } });
        },
        editar(i: number) {
            this.$store.dispatch('tarefas/editar', i);
            this.$emit('editar');
        },
        remover(i: number) {
            if (confirm('Tem realmente remover a tarefa?')) {
                this.$store.dispatch('tarefas/remover', i);
                this.$store.dispatch('alertas/showSuccessSnackBar', 'Tarefa removida com sucesso')
            }

        }
    },
    mounted() {
        this.$store.dispatch('tarefas/carregarTarefas');
    },
    computed: {
        tarefas: function() {
            return this.$store.state.tarefas.tarefas;
        }
    }
});