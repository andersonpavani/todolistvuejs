export default class Tarefa {
    titulo: string;
    descrica: string;
    prazo: Date;
    finalizado: boolean;
}