import moment from 'moment';

export default class FormatterUtil {
    static formatarData(Data: Date) {
        return moment(String(Data)).format('DD/MM/YYYY');
    }
}