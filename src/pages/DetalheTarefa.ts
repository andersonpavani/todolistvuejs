import Vue from 'vue';
import FormatterUtil from '../util/FormatterUtil'

export default Vue.component('detalhe-tarefa', {
    template:
    /* html */ `
        <div>
            <h1>Detalhe Tarefa</h1>
            <h2>Titulo da Tarefa: {{ tarefaSelecionada.titulo }}</h2>
            <p>Prazo: {{ FormatterUtil.formatarData(tarefaSelecionada.prazo) }}</p>
            <p>Descrição: {{tarefaSelecionada.descricao}}</p>
            <p>Situação: {{ tarefaSelecionada.finalizado ? 'Finalizado': 'Pendente'}}</p>
        </div>
    `,
    props: {
        tarefaSelecionada: {}
    },
    data() {
        return {
            FormatterUtil: FormatterUtil
        }
    }
});