import Vue from 'vue';
import FormularioTarefa from '../components/FormularioTarefa';
import TabelaTarefas from '../components/TabelaTarefas';

export default Vue.component('home', {
    template:
    /* html */ `
        <v-container>
            <v-layout row wrap justify-space-between class="mx-3">

                    <h1>{{titulo}}</h1>
                    <v-btn dark fab color="primary" @click="exibirFormulario = !exibirFormulario"><v-icon>{{exibirFormulario ? 'mdi-arrow-left' : 'mdi-plus'}}</v-icon></v-btn>

            </v-layout>

            <form-tarefa v-if="exibirFormulario" @voltar="exibirFormulario = false"></form-tarefa>
            <tabela-tarefas @editar="exibirFormulario = true" v-else></tabela-tarefas>
        </v-container>
    `,
    components: {
        TabelaTarefas,
        FormularioTarefa
    },
    data(){
        return {
            titulo: 'TodoList VueJS',
            exibirFormulario: false
        }
    },
    computed: {
        tituloBotao: function() {
            return this.exibirFormulario ? 'Voltar' : 'Nova Tarefa';
        }
    }
});