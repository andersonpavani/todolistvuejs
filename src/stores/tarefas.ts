import {Module} from 'vuex';
import TarefaService from '../service/TarefaService';

const module: Module<any,any> = {
    namespaced: true,
    state: {
        tarefas: [],
        indiceEdicao: null
    },
    mutations: {
        mutationTarefas(state, lista) {
            state.tarefas = lista;
        },
        mutationIndiceEdicao(state, index) {
            state.indiceEdicao = index;
        },
        mutationSalvarTarefa(state, tarefa) {
            state.tarefas[state.indiceEdicao] = tarefa;
        },
        mutationCadastraTarefa(state, tarefa) {
            state.tarefas.push(tarefa);
        },
        mutationRemover(state, index) {
            state.tarefas.splice(index, 1);
        }
    },
    actions: {
        async carregarTarefas(context) {
            let tarefas = await TarefaService.buscarTodos();
            context.commit('mutationTarefas', tarefas);
        },
        editar(context, index) {
            context.commit('mutationIndiceEdicao', index);
        },
        limparEdicao(context) {
            context.commit('mutationIndiceEdicao', null);
        },
        salvarTarefa(context, tarefa) {
            if (context.state.indiceEdicao == null) {
                context.commit('mutationCadastraTarefa', tarefa);
            } else {
                context.commit('mutationSalvarTarefa', tarefa);
            }

            TarefaService.atualizarLista(context.state.tarefas);
        },
        remover(context, index) {
            context.commit('mutationRemover', index);
            TarefaService.atualizarLista(context.state.tarefas);
        }
    },
    getters: {
        getTarefaEdicao(state) {
            if (state.indiceEdicao != null) {
                return state.tarefas[state.indiceEdicao];
            } else {
                return {};
            }
        }
    }
}

export default module;