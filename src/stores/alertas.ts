import {Module} from 'vuex';

const module: Module<any,any> = {
    namespaced: true,
    state: {
        snackbar: {
            message: '',
            color: '',
            icon: '',
            show: false
        }
    },
    mutations: {
        mutationOpenSnackBar(state, payload) {
            state.snackbar.message = payload.message;
            state.snackbar.color = payload.color;
            state.snackbar.icon = payload.icon;
            state.snackbar.show = true;
        },
        mutationCloseSnackBar(state) {
            state.snackbar.show = false;
        }
    },
    actions: {
        showSnackBar(context, payload) {
            context.commit('mutationOpenSnackBar', payload)
        },
        showSuccessSnackBar(context, message) {
            context.commit('mutationOpenSnackBar', {message: message, color: 'success', icon: 'mdi-check'})
        },
        showErrorSnackBar(context, message) {
            context.commit('mutationOpenSnackBar', {message: message, color: 'error', icon: 'mdi-close-circle-outline'})
        },
        showWarningSnackBar(context, message) {
            context.commit('mutationOpenSnackBar', {message: message, color: 'warning', icon: 'mdi-alert-outline'})
        },
        showInfoSnackBar(context, message) {
            context.commit('mutationOpenSnackBar', {message: message, color: 'info', icon: 'mdi-information-outline'})
        },
        closeSnackBar(context) {
            context.commit('mutationCloseSnackBar');
        }
    }
}

export default module;